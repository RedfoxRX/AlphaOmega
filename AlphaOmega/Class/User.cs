﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaOmega.Class
{
    class User
    {
        private static string nome;
        private static string cpf;
        private static bool logged;

        public string getNome()
        {
            return nome;
        }
        public void setNome(string Nome)
        {
            nome = Nome;
        }

        public string getCpf()
        {
            return cpf;
        }
        public void setCpf(string Cpf)
        {
            cpf = Cpf;
        }

        public bool isLogged()
        {
            return logged;
        }
        public void setLogged(bool Logged)
        {
            logged = Logged;
        }

    }
}
