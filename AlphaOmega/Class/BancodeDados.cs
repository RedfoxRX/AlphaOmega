﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.Odbc;
using AlphaOmega.Class;

namespace AlphaOmega.Class
{
    class BancodeDados
    {
        public bool verificaLogin(string cpf, string senha)
        {
            //try catch de verificacao de login
            try
            {
                OdbcConnection conexao = new Connection().Conexao();
                OdbcCommand cmd = new OdbcCommand("", conexao);
                conexao.Open();

                string sql = "select count(*) from users where cpf = " + cpf;
                cmd.CommandText = sql;
                int cpfNum = Convert.ToInt16(cmd.ExecuteScalar());

                sql = "select count(*) from users where cpf = " + cpf + " and senha = " + Sanatization(senha);
                cmd.CommandText = sql;
                int senhaNum = Convert.ToInt16(cmd.ExecuteScalar());

                conexao.Close();
                //match do cpf com o banco de dados
                if (cpfNum == 1)
                {
                    //match da senha com o banco
                    if (senhaNum == 1)
                    {//retorna true para logar
                        return true;
                    }
                    else
                    {//retorna false, sem match
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Erro! tente novamente", 
                                "Warning!",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Exclamation,
                                MessageBoxDefaultButton.Button1);
            }
            return false;
        }

        public static string Sanatization(string str)
        {
            //Função simples para evitar ataques de injeção SQL
            if (str == string.Empty || str == "")
                return str;

            string sValue = str;

            //Valores a serem substituidos
            sValue = sValue.Replace("'", "''");
            sValue = sValue.Replace("--", " ");
            sValue = sValue.Replace("/*", " ");
            sValue = sValue.Replace("*/", " ");
            sValue = sValue.Replace(" or ", "");
            sValue = sValue.Replace(" and ", "");
            sValue = sValue.Replace("update", "");
            sValue = sValue.Replace("-shutdown", "");
            sValue = sValue.Replace("--", "");
            sValue = sValue.Replace("'or'1'='1'", "");
            sValue = sValue.Replace("insert", "");
            sValue = sValue.Replace("drop", "");
            sValue = sValue.Replace("delete", "");
            sValue = sValue.Replace("xp_", "");
            sValue = sValue.Replace("sp_", "");
            sValue = sValue.Replace("select", "");
            sValue = sValue.Replace("1 union select", "");

            //Retorna o valor com as devidas alterações
            return sValue;
        }

        public void logaUsuario(string cpf)
        {
            //passa os dados do cliente para a classe user
            User user = new User();
            OdbcConnection conexao = new Connection().Conexao();
            OdbcCommand cmd = new OdbcCommand("", conexao);
            conexao.Open();

            string sql = "select nome from users where cpf = " + cpf;
            cmd.CommandText = sql;
            user.setNome(cmd.ExecuteScalar().ToString());

            sql = "select cpf from users where cpf = " + cpf;
            cmd.CommandText = sql;
            user.setCpf(cmd.ExecuteScalar().ToString());

            user.setLogged(true);

            conexao.Close();
        }
    }
}
