﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Odbc;
using System.Windows.Forms;
using AlphaOmega.Class;

namespace AlphaOmega
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            //Form Principal maximizado
            this.Width = Screen.PrimaryScreen.Bounds.Width;
            this.Height = Screen.PrimaryScreen.Bounds.Height - 40;

            this.Location = new Point();

            this.StartPosition = FormStartPosition.Manual;

        }

        private void lblFechar_Click(object sender, EventArgs e)
        {
            Environment.Exit(0);//fechar aplicacao
        }
        private void lblMinimizar_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;//minimizar aplicacao
        }
        private void lblFechar_MouseEnter(object sender, EventArgs e)
        {//mudar cor ao passar mouse em cima
            this.Cursor = Cursors.Hand;
            lblFechar.BackColor = ColorTranslator.FromHtml("#333333");
        }
        private void lblFechar_MouseLeave(object sender, EventArgs e)
        {//mudar cor ao passar mouse em cima
            this.Cursor = Cursors.Arrow;
            lblFechar.BackColor = Color.Black;
        }

        private void lblMinimizar_MouseEnter(object sender, EventArgs e)
        {//mudar cor ao passar mouse em cima
            this.Cursor = Cursors.Hand;
            lblMinimizar.BackColor = ColorTranslator.FromHtml("#333333");
        }

        private void lblMinimizar_MouseLeave(object sender, EventArgs e)
        {//mudar cor ao passar mouse em cima
            this.Cursor = Cursors.Arrow;
            lblMinimizar.BackColor = Color.Black;
        }

        private void label1_MouseEnter(object sender, EventArgs e)
        {//mudar cor ao passar mouse em cima
            this.Cursor = Cursors.Hand;
            lblNew.BackColor = ColorTranslator.FromHtml("#333333");

        }
        private void label1_MouseLeave(object sender, EventArgs e)
        {//mudar cor ao passar mouse em cima
            this.Cursor = Cursors.Arrow;
            lblNew.BackColor = Color.Black;

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            BancodeDados bd = new BancodeDados();
            
            //bd.verificaLogin();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            User u = new User();
            MessageBox.Show(u.getNome() + "\n" + u.getCpf() + "\n" + u.isLogged());
            
        }
    }   
}
